# bl_info = {
#     "name": "PUP Gp Show #trt layers",
#     "blender": (3, 4, 0),
#     "version": (0, 1),
#     "author": "Cedric NICOLAS www.cedriclefox.com",
#     "category": "Object",
# }

"""
Change vertex color, based on gamma and saturation algo

todo
- Add frame option. For now, it evaluate on all frames

By Cedric3d
nicolasced(AT)gmail.com
www.puppetsoul.com
www.cedriclefox.com

"""


import bpy

class PUP_Gp_OP_show_trt_layers(bpy.types.Operator):
    """Operator to change the vertex colors from a Grease Pencil object"""
    bl_idname = "object.pup_gp_show_trt_layers"
    bl_label = "PUP Gp - Show #trt layers"
    bl_options = {'REGISTER', 'UNDO'}
    
    apply_to_selected: bpy.props.BoolProperty(
        name="Selected Obj Only",
        default=True,
        options={'SKIP_SAVE'}
    )
    
    HThide: bpy.props.BoolProperty(
        name="trt",
        default=True,
        options={'SKIP_SAVE'}
    )        
    @classmethod
    def poll(cls, context):
        return context.selected_objects is not None
    
    def draw(self, context):
        layout = self.layout
        col0 = layout.column()
        col0.label(text="Apply to:")
        col0.prop(self, "apply_to_selected")

        col0.label(text=" ")
        col0.prop(self, "HThide")

         
    
    
    def execute(self, context):
        # Define the remove_vertex_paint function
        def showLayers(gpencil: bpy.types.GreasePencil, HThide: bool):
            
            for layer in gpencil.layers:

                if "#trt" in layer.info:
                    layer.hide = False
                else:
                    layer.hide = True

                print(layer.info)
                # print(layer.name)


        # If the apply_to_selected property is True, remove the vertex colors from the selected objects
        count_process = 0
        if self.apply_to_selected:
            for obj in context.selected_objects:
                if obj.type == 'GPENCIL':
                    showLayers(obj.data, self.HThide)
                    count_process += 1
        # Otherwise, remove the vertex colors from all objects
        else:
            for gp in bpy.data.grease_pencils:
                showLayers(gp, self.HThide)
                count_process += 1
                
        if count_process == 0:
            self.report({'WARNING'}, 'Info message')
        
        return {'FINISHED'}

# Register the operator
# bpy.utils.register_class(ChangeVertexColorsOperator)


def menu_func(self, context):
    self.layout.operator(PUP_Gp_OP_show_trt_layers.bl_idname)

def register():
    bpy.utils.register_class(PUP_Gp_OP_show_trt_layers)
    bpy.types.VIEW3D_MT_object.append(menu_func)  # Adds the new operator to an existing menu.
    bpy.types.VIEW3D_MT_draw_gpencil.append(menu_func) 
    
    bpy.types.GPENCIL_MT_layer_context_menu.append(menu_func) 

def unregister():
    bpy.utils.unregister_class(PUP_Gp_OP_show_trt_layers)