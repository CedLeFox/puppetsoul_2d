# bl_info = {
#     "name": "PUP rename Scene Object",
#     "blender": (3, 4, 0),
#     "version": (0, 1),
#     "author": "Cedric NICOLAS www.cedriclefox.com",
#     "category": "Object",
# }

"""
rename obj of a scene

By Cedric3d
nicolasced(AT)gmail.com
www.puppetsoul.com
www.cedriclefox.com

"""

import bpy


class PUP_Gp_OP_rename_Scene_Obj(bpy.types.Operator):
    """Operator to reproject and keeping the same size"""
    bl_idname = "object.pup_rename_scene_obj"
    bl_label = "PUP Rename scene objects (OBJ->SCN)"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return context.selected_objects is not None
        
    # ---------------------------------------------------
    def execute(self, context):

        # Get the current scene
        scene = bpy.context.scene

        # Get the name of the current scene
        suffix = scene.name
        print("--")
        # Add the suffix to all objects in the scene
        for obj in scene.objects:
            if "->" in obj.name: obj.name = obj.name.split("->")[0] 
            obj.name = obj.name + "->" + suffix
            try :
                obj.data.name = obj.name
            except:
                pass

        # Add the suffix to all collections in the scene
        for collection in scene.collection.children:
            if scene.user_of_id(collection):
                print(collection)
                if "->" in collection.name: collection.name = collection.name.split("->")[0] 
                collection.name = collection.name + "->" + suffix

        return {'FINISHED'}


# Register the operator
# bpy.utils.register_class(ProjectStrokeOperator)

def menu_func(self, context):
    self.layout.operator(PUP_Gp_OP_rename_Scene_Obj.bl_idname)
    self.layout.separator()

def register():
    bpy.utils.register_class(PUP_Gp_OP_rename_Scene_Obj)
    bpy.types.VIEW3D_MT_object.append(menu_func)  # Adds the new operator to an existing menu.

def unregister():
    bpy.utils.unregister_class(PUP_Gp_OP_rename_Scene_Obj)