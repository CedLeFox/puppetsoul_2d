# bl_info = {
#     "name": "PUP Gp Change Vertex Color",
#     "blender": (3, 4, 0),
#     "version": (0, 1),
#     "author": "Cedric NICOLAS www.cedriclefox.com",
#     "category": "Object",
# }

"""
Change vertex color, based on gamma and saturation algo

todo
- Add frame option. For now, it evaluate on all frames

By Cedric3d
nicolasced(AT)gmail.com
www.puppetsoul.com
www.cedriclefox.com

"""


import bpy
import colorsys

class PUP_Gp_OP_Change_vertex_color(bpy.types.Operator):
    """Operator to change the vertex colors from a Grease Pencil object"""
    bl_idname = "object.pup_gp_change_vertex_colors"
    bl_label = "PUP Gp - Change Vertex Colors"
    bl_options = {'REGISTER', 'UNDO'}
    
    current_frame_only: bpy.props.BoolProperty(
        name="Current Frame Only",
        default=False,
        options={'SKIP_SAVE'}
    )

    apply_to_selected: bpy.props.BoolProperty(
        name="Selected Obj Only",
        default=True,
        options={'SKIP_SAVE'}
    )
    
    apply_only_to_unlock_layers: bpy.props.BoolProperty(
        name="Unlock Layers Only",
        default=True,
        options={'SKIP_SAVE'}
    )
    
    apply_only_to_selected_layer: bpy.props.BoolProperty(
        name="Selected Layer Only",
        default=True,
        options={'SKIP_SAVE'}
    )


    off: bpy.props.BoolProperty(
        name="off",
        default=True,
        options={'SKIP_SAVE'}
    )

    saturation_blend_factor: bpy.props.FloatProperty(
        name="Saturation",
        default=1.0,
        min=0.0,
        max=10.0,
        options={'SKIP_SAVE'}
    )
    gamma_R: bpy.props.FloatProperty(
        name="Gamma R",
        default=1.0,
        min=0.001,
        max=10.0,
        options={'SKIP_SAVE'}
    )
    gamma_G: bpy.props.FloatProperty(
        name="Gamma G",
        default=1.0,
        min=0.001,
        max=10.0,
        options={'SKIP_SAVE'}
    )
    gamma_B: bpy.props.FloatProperty(
        name="Gamma B",
        default=1.0,
        min=0.001,
        max=10.0,
        options={'SKIP_SAVE'}
    )    


    color_tint : bpy.props.FloatVectorProperty(
        name="Color Tint",
        subtype='COLOR', 
        min=0.0, 
        max=1.0,
        size=3) # Alpha

    color_tint_factor: bpy.props.FloatProperty(
        name="Color Tint Factor",
        default=1.0,
        min=0,
        max=1,
        options={'SKIP_SAVE'}
    )

    use_hsv: bpy.props.BoolProperty(
        name="use HSV (vs HSL)",
        default=False,
        options={'SKIP_SAVE'}
    )   


    debug: bpy.props.BoolProperty(
        name="debug",
        default=False,
        options={'SKIP_SAVE'}
    )        
    @classmethod
    def poll(cls, context):
        return context.selected_objects is not None
    
    def draw(self, context):
        layout = self.layout
        col0 = layout.column()
        col0.label(text="Apply to:")
        col1 = layout.column()
                
        col1.prop(self, "apply_to_selected")
        col1.prop(self, "apply_only_to_unlock_layers")
        col2 = layout.column()
        col2.prop(self, "apply_only_to_selected_layer")
        col3 = layout.column()
        col3.prop(self, "current_frame_only")
        
        col4 = layout.column()
        col4.prop(self, "color_tint")
        col4.prop(self, "color_tint_factor")
        col4.prop(self, "use_hsv")
        col4.prop(self, "saturation_blend_factor")
        col4.prop(self, "gamma_R")
        col4.prop(self, "gamma_G")
        col4.prop(self, "gamma_B")
        # col4.label(text=" ")
        col4.label(text=" ")
        col4.prop(self, "off")
        col4.prop(self, "debug")

    
        # If the apply_only_to_selected_layer property is True, gray out the other two properties
        if self.apply_only_to_selected_layer:
            col1.enabled = False
        else:
            col1.enabled = True
            
    
    
    def execute(self, context):
        # Define the remove_vertex_paint function
        def desaturate_vertex_paint(gpencil: bpy.types.GreasePencil, blend_factor: float, gamma_R:float, gamma_G:float, gamma_B:float, current_frame_only:bool, debug: bool):

            # get frame            
            scene = bpy.context.scene
            current_frame = scene.frame_current
            current_frame = int(current_frame)
            #print("FRANE", current_frame)

            done = False

            for layer in gpencil.layers:
                
                # because layer.select doesnt work
                current_layer = bpy.context.active_object.data.layers.active.info
                is_layer_selected = False
                if current_layer == layer.info:
                    is_layer_selected = True

                if debug: print(" - Check Layer : ", gpencil.name, " > ", layer, "| sel/lock : ",  is_layer_selected, layer.lock)

                # apply
                if (self.apply_only_to_unlock_layers and not self.apply_only_to_selected_layer and not layer.lock) or (self.apply_only_to_selected_layer and is_layer_selected) or (not self.apply_only_to_selected_layer and (  (self.apply_only_to_unlock_layers and not layer.lock) or not self.apply_only_to_unlock_layers) ) :
                    if debug: print(" ---> Process! ")
                    #print(len(layer.frames))

                    at_least_one_fr_sel = False
                    for frame in layer.frames:
                        if frame.select: at_least_one_fr_sel = True

                    # frames
                    for it, frame in enumerate(layer.frames):

                        # test conditions for apply on frames
                        frame_do = False
                        if current_frame_only:
                            if frame.frame_number == current_frame or (it == len(layer.frames)-1 and current_frame > layer.frames[-1].frame_number):
                                frame_do = True
                            elif frame.frame_number < current_frame and current_frame < layer.frames[it+1].frame_number:
                                frame_do = True
                            else:
                                frame_do = False
                        else:
                            if frame.select and at_least_one_fr_sel == True:
                                frame_do = True
                            if at_least_one_fr_sel == False:
                                frame_do = True
                        
                        #print("   - ", frame.frame_number, current_frame, frame_do)
                        

                        if frame_do:
                            if debug: print(" ---> Process this frame! ", frame.frame_number)
                            for stroke in frame.strokes:

                                stroke_do = False
                                mode = bpy.context.active_object.mode
                                # print(mode)
                                if mode == "EDIT_GPENCIL": 
                                    if stroke.select: stroke_do=True
                                else:
                                    stroke_do=True

                                if stroke_do:
                                    #stroke.vertex_color_fill = [0.0, 0.0, 0.0, 0.0]
                                    # print(stroke.select)
                                    i = 0
                                    for point in stroke.points:
                                        done = True
                                        cl = point.vertex_color

                                        # apply color tint
                                        if self.color_tint[0] + self.color_tint[1] + self.color_tint[2] != 0:

                                            if self.use_hsv:
                                                user_hsv = colorsys.rgb_to_hsv(cl[0], cl[1], cl[2])
                                                goal_hsv = colorsys.rgb_to_hsv(self.color_tint[0],self.color_tint[1],self.color_tint[2])
                                                result_rgb = colorsys.hsv_to_rgb(goal_hsv[0], goal_hsv[1], user_hsv[2])
                                            else:
                                                user_hls = colorsys.rgb_to_hls(cl[0], cl[1], cl[2])
                                                goal_hls = colorsys.rgb_to_hls(self.color_tint[0],self.color_tint[1],self.color_tint[2])
                                                result_rgb = colorsys.hls_to_rgb(goal_hls[0], user_hls[1], goal_hls[2])

                                            for j in range(3):
                                                cl[j] = (self.color_tint_factor * result_rgb[j]) + ((1-self.color_tint_factor) * cl[j])
                                        # color tint end


                                        # apply gama and saturation
                                        gray_value = 0.2999*cl[0] + 0.587*cl[1] + 0.114*cl[2]
                                        if debug: 
                                            if i == 0: print("    -> ", cl[0], cl[1], cl[2])

                                        cl[0] = ((1-blend_factor) * gray_value + blend_factor * cl[0]) ** (1/ (gamma_R))
                                        cl[1] = ((1-blend_factor) * gray_value + blend_factor * cl[1]) ** (1/ (gamma_G))
                                        cl[2] = ((1-blend_factor) * gray_value + blend_factor * cl[2]) ** (1/ (gamma_B))
                                        # apply gama and saturation end


                                        if debug: 
                                            if i == 0: print("    >> ", cl[0], cl[1], cl[2])

                                        # end it
                                        point.vertex_color = [ cl[0] , cl[1] , cl[2] , cl[3] ]
                                        i = i+1
                                        #--
            # return
            return done

        # -----------------------------------------------
        # If the apply_to_selected property is True, remove the vertex colors from the selected objects

        if self.off:
            self.report({'WARNING'}, 'Off!')
            return {'FINISHED'}

        count_process = 0
        if self.apply_to_selected:
            for obj in context.selected_objects:
                if obj.type == 'GPENCIL':
                    result = desaturate_vertex_paint(obj.data, self.saturation_blend_factor, self.gamma_R, self.gamma_G, self.gamma_B, self.current_frame_only, self.debug)
                    if result: count_process += 1
        # Otherwise, remove the vertex colors from all objects
        else:
            for gp in bpy.data.grease_pencils:
                result = desaturate_vertex_paint(gp, self.saturation_blend_factor, self.gamma_R, self.gamma_G, self.gamma_B, self.current_frame_only, self.debug)
                if result: count_process += 1
                
        if count_process == 0:
            self.report({'WARNING'}, 'Nothing processed. Check your options')
        
        return {'FINISHED'}

# Register the operator
# bpy.utils.register_class(PUP_Gp_OP_Change_vertex_color)

def menu_func(self, context):
    self.layout.separator()
    self.layout.operator(PUP_Gp_OP_Change_vertex_color.bl_idname)

def register():
    bpy.utils.register_class(PUP_Gp_OP_Change_vertex_color)
    bpy.types.VIEW3D_MT_object.append(menu_func)  # Adds the new operator to an existing menu.
    bpy.types.VIEW3D_MT_draw_gpencil.append(menu_func) 
    bpy.types.VIEW3D_MT_edit_gpencil_stroke.append(menu_func) 

def unregister():
    bpy.utils.unregister_class(PUP_Gp_OP_Change_vertex_color)
    pass