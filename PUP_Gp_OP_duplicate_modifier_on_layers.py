# bl_info = {
#     "name": "PUP Gp Duplicate modifier on layers",
#     "blender": (3, 4, 0),
#     "version": (0, 1),
#     "author": "Cedric NICOLAS www.cedriclefox.com",
#     "category": "Object",
# }


"""
Select stroke by color


By Cedric3d
nicolasced(AT)gmail.com
www.puppetsoul.com
www.cedriclefox.com

"""

import bpy

class PUP_Gp_OP_duplicate_modifier_on_layers(bpy.types.Operator):
    """Duplicate modifier on layers"""
    bl_idname = "object.pup_gp_duplicate_modifier_on_layers"
    bl_label = "PUP Gp - Duplicate modifier on layers"
    bl_options = {'REGISTER', 'UNDO'}
    
    include_string: bpy.props.StringProperty(
        name="include string",
        default="",
        options={'SKIP_SAVE'}
    )

    off: bpy.props.BoolProperty(
        name="off",
        default=True,
        options={'SKIP_SAVE'}
    )        
    @classmethod
    def poll(cls, context):
        return context.selected_objects is not None
    
    def draw(self, context):
        layout = self.layout
        col0 = layout.column()
        col0.label(text="Apply to:")
        col0.prop(self, "include_string")
        col0.prop(self, "off")

    # main
    def execute(self, context):
        # Define the remove_vertex_paint function

        if self.off:
            self.report({'WARNING'}, 'Off!')
            return {'FINISHED'}
        
        if len(self.include_string) == 0:
            self.report({'WARNING'}, 'Please fill the string!')
            return {'FINISHED'}           

        active_object = bpy.context.active_object
        if active_object.type != 'GPENCIL':
            self.report({'ERROR'}, 'Please select GP obj')

        modifs = active_object.grease_pencil_modifiers
        print(modifs)
        gpencil = active_object.data

        modif_to_copy = False
        # get modifier
        all_modifiers = []
        for modif in list(modifs):
            all_modifiers.append(modif.name)
            if "COPY" in modif.name:
                modif_to_copy = modif
                print("FOUND TO COPY : ", modif)

        if not modif_to_copy:
            self.report({'ERROR'}, 'Cannot find a modifier with COPY in the name')
            return {'FINISHED'}   

        for layer in gpencil.layers:

            if self.include_string in layer.info:
                
                print("Layer to apply the duplication : ", layer)
                bpy.ops.object.gpencil_modifier_copy(modifier=modif_to_copy.name)
                modifs_new_added = active_object.grease_pencil_modifiers
                for m in list(modifs_new_added):

                    if m.name not in all_modifiers:

                        m.name = m.name + ">" + layer.info
                        m.layer = layer.info

                        all_modifiers.append(m.name)
                        #print("the new modif is ", m)
                    

        return {'FINISHED'}

# Register the operator
# bpy.utils.register_class(ChangeVertexColorsOperator)

def menu_func(self, context):
    self.layout.operator(PUP_Gp_OP_duplicate_modifier_on_layers.bl_idname)

def register():
    bpy.utils.register_class(PUP_Gp_OP_duplicate_modifier_on_layers)
    #bpy.types.VIEW3D_MT_object.append(menu_func)  # Adds the new operator to an existing menu.
    bpy.types.VIEW3D_MT_object.append(menu_func) 

def unregister():
    bpy.utils.unregister_class(PUP_Gp_OP_duplicate_modifier_on_layers)