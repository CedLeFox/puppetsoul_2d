# bl_info = {
#     "name": "PUP Gp Color picker",
#     "blender": (3, 4, 0),
#     "version": (0, 1),
#     "author": "Cedric NICOLAS www.cedriclefox.com",
#     "category": "Object",
# }

import bpy
from bpy.types import Operator
import mathutils
from mathutils import Vector, Matrix, geometry
from bpy_extras import view3d_utils
from time import time
#from .utils import get_gp_draw_plane, location_to_region, region_to_location

class PUP_Gp_OP_Color_picker(Operator):
    bl_idname = "gp.pick_color"
    bl_label = "PUP Gp - Get Closest Color"
    bl_description = "PUP Gp Pick closest stroke layer"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type == 'GPENCIL' and context.mode == 'PAINT_GPENCIL'

    stroke_filter : bpy.props.EnumProperty(name='Target',
        default='STROKE',
        items=(
        ('STROKE', 'Stroke', 'Target only Stroke', 0),   
        ('FILL', 'Fill', 'Target only Fill', 1),   
        ('ALL', 'All', 'All stroke types', 2),
        ),
        options={'SKIP_SAVE'})

    def filter_stroke(self, context):
        kd = mathutils.kdtree.KDTree(len(self.point_pair))
        for i, pair in enumerate(self.point_pair):
            kd.insert(pair[0], i)
        kd.balance()

        mouse_vec3 = Vector((*self.init_mouse, 0))
        co, index, _dist = kd.find(mouse_vec3)
        lid = self.point_pair[index][1]

        ##
        color_linear = list(self.point_color_pair[index])
        color_linear_COLOR = mathutils.Color(color_linear[:3])

        color_sRGB = color_linear_COLOR.from_scene_linear_to_srgb()

        print(list(color_sRGB))

        color_srgb = mathutils.Color(color_linear[:3]).from_scene_linear_to_srgb()

        return lid

    def invoke(self, context, event):
        self.t0 = time()
        self.limit = self.t0 + 0.2 # 200 miliseconds
        self.init_mouse = Vector((event.mouse_region_x, event.mouse_region_y))
        self.idx = None
        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}

    def draw(self, context):
        layout = self.layout
        if context.object.data.layers.active:
            layout.label(text=f'Layer: {context.object.data.layers.active.info}')
        layout.prop(self, 'stroke_filter')

    def modal(self, context, event):
        if time() > self.limit:
            return {'CANCELLED'}

        if event.value == 'RELEASE': # if a key was release (any key in case shortcut was customised)
            if time() > self.limit:
                # dont know if condition is neeed
                return {'CANCELLED'}
            
            return self.execute(context)
            # return {'FINISHED'}

        return {'PASS_THROUGH'}
        # return {'RUNNING_MODAL'}

    def execute(self, context):
        t1 = time()
        # self.prefs = get_addon_prefs()
        self.ob = context.object
        mat = self.ob.matrix_world 
        gp = self.ob.data

        self.inv_mat = self.ob.matrix_world.inverted()
        self.point_pair = []
        self.point_color_pair = []
        if gp.use_multiedit:
            for layer_id, l in enumerate(gp.layers):
                if l.hide:# l.lock or 
                    continue
                for f in l.frames:
                    if not f.select:
                        continue
                    for s in f.strokes:
                        if self.stroke_filter == 'STROKE' and not self.ob.data.materials[s.material_index].grease_pencil.show_stroke:
                            continue
                        elif self.stroke_filter == 'FILL' and not self.ob.data.materials[s.material_index].grease_pencil.show_fill:
                            continue
                        self.point_pair += [(Vector((*location_to_region(mat @ p.co), 0)), layer_id) for p in s.points]
                        

        else:
            # [s for l in gp.layers if not l.lock and not l.hide for s in l.active_frame.stokes]
            for layer_id, l in enumerate(gp.layers):
                if l.hide or not l.active_frame:# l.lock or 
                    continue
                for s in l.active_frame.strokes:
                    if self.stroke_filter == 'STROKE' and not self.ob.data.materials[s.material_index].grease_pencil.show_stroke:
                        continue
                    elif self.stroke_filter == 'FILL' and not self.ob.data.materials[s.material_index].grease_pencil.show_fill:
                        continue
                    self.point_pair += [(Vector((*location_to_region(mat @ p.co), 0)), layer_id) for p in s.points]
                    self.point_color_pair += [p.vertex_color for p in s.points]
   

        if not self.point_pair:
            self.report({'ERROR'}, 'No stroke found, maybe layers are locked or hidden')
            return {'CANCELLED'}


        lid = self.filter_stroke(context)
        
        if isinstance(lid, str):
            self.report({'ERROR'}, lid)
            return {'CANCELLED'}

        print(self.point_pair)
        del self.point_pair # auto garbage collected ?

        self.ob.data.layers.active_index = lid
        
        ## debug show trigger time
        # print(f'Trigger time   {time() - self.t0:.3f}')
        # print(f'Search time   {time() - t1:.3f}')
        self.report({'INFO'}, f'Layer: {self.ob.data.layers.active.info}')
        return {'FINISHED'}


def location_to_region(worldcoords):
    from bpy_extras import view3d_utils
    return view3d_utils.location_3d_to_region_2d(bpy.context.region, bpy.context.space_data.region_3d, worldcoords)

def region_to_location(viewcoords, depthcoords):
    from bpy_extras import view3d_utils
    return view3d_utils.region_2d_to_location_3d(bpy.context.region, bpy.context.space_data.region_3d, viewcoords, depthcoords)

addon_keymaps = []
def register_keymaps():
    addon = bpy.context.window_manager.keyconfigs.addon
    km = addon.keymaps.new(name = "Grease Pencil Stroke Paint Mode Color Picker", space_type = "EMPTY", region_type='WINDOW')
    
    kmi = km.keymap_items.new(
        # name="",
        idname="gp.pick_color",
        type="E",
        #ctrl = True,
        value="PRESS",
        )
    kmi.properties.stroke_filter = 'STROKE'
    addon_keymaps.append((km, kmi))

def unregister_keymaps():
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()


classes=(
PUP_Gp_OP_Color_picker,
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    register_keymaps()

def unregister():
    unregister_keymaps()
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)