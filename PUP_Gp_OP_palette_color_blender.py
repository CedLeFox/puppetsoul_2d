# bl_info = {
#     "name": "PUP Gp palette Color Blend",
#     "blender": (3, 4, 0),
#     "version": (0, 1),
#     "author": "Cedric NICOLAS www.cedriclefox.com",
#     "category": "Object",
# }

"""
Blend between the primary and the secondary color, adding 8 new colors to the palette


By Cedric3d
nicolasced(AT)gmail.com
www.puppetsoul.com
www.cedriclefox.com
"""


import bpy
from mathutils import Color


class PUP_Gp_OP_palette_color_blender(bpy.types.Operator):
    bl_idname = 'object.pup_gp_palette_color_blend'
    bl_label = 'PUP Gp - Palette Color Blender'
    bl_description = 'Blend Color from primary to secondary'
    bl_options = {'REGISTER'}

    def execute(self, context):

        # Print the name of the current brush
        #brush = bpy.context.scene.tool_settings.gpencil_paint.brush.name
        #bpy.data.brushes[brush].color = rgb_as_list

        brush = bpy.context.scene.tool_settings.gpencil_paint.brush.name
        color1 = bpy.data.brushes[brush].color
        color2 = bpy.data.brushes[brush].secondary_color

        print(color1)
        print(color2)

        for i in range(9):

            #step = i/11.0  # Blend halfway between the two colors

            step = self.remap(i, 0, 8, 0, 1)

            r = color1.r + (color2.r - color1.r) * step
            g = color1.g + (color2.g - color1.g) * step
            b = color1.b + (color2.b - color1.b) * step

            blended_color = Color((r, g, b))

            print(i)
            print(step)
            print(blended_color)

            bpy.data.brushes[brush].color = blended_color

            bpy.ops.palette.color_add()

        return {'FINISHED'}


    def remap(self, old_value, old_min, old_max, new_min, new_max):
        return (old_value - old_min) * (new_max - new_min) / (old_max - old_min) + new_min


def menu_func(self, context):
    self.layout.operator(PUP_Gp_OP_palette_color_blender.bl_idname)

def register():
    bpy.utils.register_class(PUP_Gp_OP_palette_color_blender)
    #bpy.types.VIEW3D_MT_object.append(menu_func)  # Adds the new operator to an existing menu.
    bpy.types.VIEW3D_MT_draw_gpencil.append(menu_func) 

def unregister():
    bpy.utils.unregister_class(PUP_Gp_OP_palette_color_blender)