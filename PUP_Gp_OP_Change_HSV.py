# bl_info = {
#     "name": "PUP Gp change Hsv",
#     "blender": (3, 4, 0),
#     "version": (0, 1),
#     "author": "Cedric NICOLAS www.cedriclefox.com",
#     "category": "Object",
# }

"""
Change HSC of the current color for brush/vertex color paint/gp

todo
- swap secondary/primary color
- keep lightness

By Cedric3d
nicolasced(AT)gmail.com
www.puppetsoul.com
www.cedriclefox.com

"""


import bpy
import colorsys
import mathutils



class PUP_Gp_OP_Change_HSV(bpy.types.Operator):
    bl_idname = 'object.pup_gp_change_hsv'
    bl_label = 'PUP Gp - Change Hsv'
    bl_description = 'Change Hsv'
    bl_options = {'REGISTER', 'UNDO'}


    # color : bpy.props.FloatVectorProperty(
    #     name="Color",
    #     subtype='COLOR', 
    #     min=0.0, 
    #     max=1.0,
    #     size=3) # Alpha   

    Step: bpy.props.FloatProperty(
        name="Step",
        default=0.01,
        min=0.0001,
        max=0.3,
        options={'SKIP_SAVE'}
    )

 

    mode: bpy.props.StringProperty(default = 'H+')

    @classmethod
    def poll(cls, context):
        return context.selected_objects is not None
    
    def draw(self, context):
        layout = self.layout
        col0 = layout.column()
        # col0.prop(self, "color")
        col0.prop(self, "Step")
        # col0.prop(self, "color")

    def execute(self, context):

        step = self.Step

        # Print the name of the current brush
        #brush = bpy.context.scene.tool_settings.gpencil_paint.brush.name
        #bpy.data.brushes[brush].color = rgb_as_list

        brush = bpy.context.scene.tool_settings.gpencil_paint.brush.name
        color1 = bpy.data.brushes[brush].color
        #color2 = bpy.data.brushes[brush].secondary_color

        print(color1)
        #print(color2)

        color1_list = list(color1)

        # Convert the RGB values to HSV values
        h, s, v = colorsys.rgb_to_hsv(color1_list[0], color1_list[1], color1_list[2])

        print("---")
        print(h, s, v)

        if self.mode == "H+":
            h = h + step
            self.report({'INFO'}, 'New H : '+ str(round(h,3)) ) 
        if self.mode == "H-":
            h = h - step
            self.report({'INFO'}, 'New H : '+ str(round(h,3)) ) 

        if self.mode == "S+":
            s = s + step
            self.report({'INFO'}, 'New S : '+ str(round(s,3)) ) 
        if self.mode == "S-":
            s = s - step
            self.report({'INFO'}, 'New S : '+ str(round(s,3)) ) 

        if self.mode == "V+":
            v = v + step
            self.report({'INFO'}, 'New V : '+ str(round(v,3)) ) 
        if self.mode == "V-":
            v = v - step
            self.report({'INFO'}, 'New V : '+ str(round(v,3)) ) 

        r,g,b = colorsys.hsv_to_rgb(h, s, v)

        color1 = bpy.data.brushes[brush].color = [r,g,b]

        # color_srgb = mathutils.Color([r,g,b]).from_scene_linear_to_srgb()

        # self.color=color_srgb
        # Print the HSV values
        print(h, s, v)

        

        return {'FINISHED'}


    def remap(self, old_value, old_min, old_max, new_min, new_max):
        return (old_value - old_min) * (new_max - new_min) / (old_max - old_min) + new_min


def menu_func(self, context):
    self.layout.separator()

    km = bpy.context.window_manager.keyconfigs.addon.keymaps.new(name='3D View', space_type='VIEW_3D')
    
    kmi = km.keymap_items.new(PUP_Gp_OP_Change_HSV.bl_idname, 'F6', 'PRESS', shift=True, ctrl=True)
    kmi.properties.mode = 'H+'  
    self.layout.operator(PUP_Gp_OP_Change_HSV.bl_idname,  text="H+").mode = 'H+'

    kmi = km.keymap_items.new(PUP_Gp_OP_Change_HSV.bl_idname, 'F5', 'PRESS', shift=True, ctrl=True)
    kmi.properties.mode = 'H-'
    self.layout.operator(PUP_Gp_OP_Change_HSV.bl_idname,  text="H-").mode = 'H-'

    kmi = km.keymap_items.new(PUP_Gp_OP_Change_HSV.bl_idname, 'F8', 'PRESS', shift=True, ctrl=True)
    kmi.properties.mode = 'S+'  
    self.layout.operator(PUP_Gp_OP_Change_HSV.bl_idname,  text="S+").mode = 'S+'

    kmi = km.keymap_items.new(PUP_Gp_OP_Change_HSV.bl_idname, 'F7', 'PRESS', shift=True, ctrl=True)
    kmi.properties.mode = 'S-'  
    self.layout.operator(PUP_Gp_OP_Change_HSV.bl_idname,  text="S-").mode = 'S-'

    kmi = km.keymap_items.new(PUP_Gp_OP_Change_HSV.bl_idname, 'F10', 'PRESS', shift=True, ctrl=True)
    kmi.properties.mode = 'V+'  
    self.layout.operator(PUP_Gp_OP_Change_HSV.bl_idname,  text="V+").mode = 'V+'

    kmi = km.keymap_items.new(PUP_Gp_OP_Change_HSV.bl_idname, 'F9', 'PRESS', shift=True, ctrl=True)
    kmi.properties.mode = 'V-'  
    self.layout.operator(PUP_Gp_OP_Change_HSV.bl_idname,  text="V-").mode = 'V-'



def register():
    bpy.utils.register_class(PUP_Gp_OP_Change_HSV)
    #bpy.types.VIEW3D_MT_object.append(menu_func)  # Adds the new operator to an existing menu.
    bpy.types.VIEW3D_MT_draw_gpencil.append(menu_func)


def unregister():
    bpy.utils.unregister_class(PUP_Gp_OP_Change_HSV)