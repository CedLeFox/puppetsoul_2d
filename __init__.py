bl_info = {
    "name": "Puppet Soul - 2D tools",
    "description": "'Puppet Soul' Tools for 2d anim productions with Grease Pencil",
    "author": "Cedric NICOLAS",
    "version": (1, 0, 0),
    "blender": (3, 5, 0),
    "location": "Sidebar (N menu) > PUP GP // divers menus",
    "warning": "",
    "doc_url": "",
    "tracker_url": "",
    "category": "Grease Pencil",
}

# test

from importlib import reload
import bpy

# import
from . import PUP_Gp_OP_Change_HSV
from . import PUP_Gp_OP_Change_vertex_color
from . import PUP_Gp_OP_Color_picker
from . import PUP_Gp_OP_depth_cam_move
from . import PUP_Gp_OP_duplicate_modifier_on_layers
from . import PUP_Gp_OP_palette_color_blender
from . import PUP_Gp_OP_playblast_offline
from . import PUP_Gp_OP_project_stroke
from . import PUP_Gp_OP_rename_Scene_Obj
from . import PUP_Gp_OP_select_by_color
from . import PUP_Gp_OP_show_ruf_layers
from . import PUP_Gp_OP_show_trt_layers
from . import PUP_Gp_OP_show_user_layers
from . import PUP_Gp_OP_Get_Closest_Layer

# reload
reload(PUP_Gp_OP_Change_HSV)
reload(PUP_Gp_OP_Change_vertex_color)
reload(PUP_Gp_OP_Color_picker)
reload(PUP_Gp_OP_depth_cam_move)
reload(PUP_Gp_OP_duplicate_modifier_on_layers)
reload(PUP_Gp_OP_palette_color_blender)
reload(PUP_Gp_OP_playblast_offline)
reload(PUP_Gp_OP_project_stroke)
reload(PUP_Gp_OP_rename_Scene_Obj)
reload(PUP_Gp_OP_select_by_color)
reload(PUP_Gp_OP_show_ruf_layers)
reload(PUP_Gp_OP_show_trt_layers)
reload(PUP_Gp_OP_show_user_layers)
reload(PUP_Gp_OP_Get_Closest_Layer)

# modules
addon_modules = (
    PUP_Gp_OP_Change_HSV,
    PUP_Gp_OP_Change_vertex_color,
    PUP_Gp_OP_Color_picker,
    PUP_Gp_OP_depth_cam_move,
    PUP_Gp_OP_duplicate_modifier_on_layers,
    PUP_Gp_OP_palette_color_blender,
    PUP_Gp_OP_playblast_offline,
    PUP_Gp_OP_project_stroke,
    PUP_Gp_OP_rename_Scene_Obj,
    PUP_Gp_OP_select_by_color,
    PUP_Gp_OP_show_ruf_layers,
    PUP_Gp_OP_show_trt_layers,
    PUP_Gp_OP_show_user_layers,
    PUP_Gp_OP_Get_Closest_Layer,
    )

# register
def register():
    for mod in addon_modules:
        mod.register()

# unregister
def unregister():
    for mod in reversed(addon_modules):
        mod.unregister()

# final
if __name__ == "__main__":
    register()
