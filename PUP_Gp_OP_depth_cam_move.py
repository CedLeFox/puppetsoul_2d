# bl_info = {
#     "name": "PUP Depth cam move",
#     "blender": (3, 4, 0),
#     "version": (0, 1),
#     "category": "Object",
# }

import bpy
import bmesh

from mathutils import Vector
from mathutils import Matrix

class PUP_Gp_OP_depth_cam_move(bpy.types.Operator):
    bl_idname = "object.depth_cam_move"
    bl_label = "PUP Depth cam move"
    bl_description = "Move object in the depth from camera POV while retaining same size in framing"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type != 'CAMERA' # and context.scene.camera

    def invoke(self, context, event):
        self.init_mouse_x = event.mouse_x
        self.cam = bpy.context.scene.camera
        if not self.cam:
            self.report({'ERROR'}, 'No active camera')
            return {"CANCELLED"}

        self.cam_pos = self.cam.matrix_world.translation
        self.mode = 'distance'
        self.objects = [o for o in context.selected_objects if o.type != 'CAMERA']

        # -- add -- 
        # i fedit mode, then gop to bmash and record vertices
        self.object_Mode = True
        print(f"{self.objects=}")
        if bpy.context.active_object.mode == "EDIT":
            print("edit mode")

            # set flag
            self.object_Mode = False
            object = bpy.context.active_object

            # -----------
            listver = []
            # bmesh object
            self.object = object
            self.odata = bmesh.from_edit_mesh(self.object.data)
            self.odata.select_flush(False)
            self.odata.verts.ensure_lookup_table()

            # -----------------
            # if the vertex is selected add to the list
            for vertice in self.odata.verts[:]:
                if vertice.select:
                    listver.append(vertice.index)

            # record the list in object var
            self.objects = listver

        else:
            print("obj mode")


        # obj mode
        if self.object_Mode:
            self.init_mats = [o.matrix_world.copy() for o in self.objects]
        # edit mode
        else:
            # self.init_mats = []
            # for v in self.objects:

            #     co_final = self.objects_in_obj_mode.matrix_world @ v.co

            #     self.init_mats.append( Matrix.Translation(Vector(co_final)))
                #self.init_mats = [o.matrix_world.copy() for o in self.objects]
            pass
        
        # -- add end -- 
        
        if self.cam.data.type == 'ORTHO':
            context.area.header_text_set(f'Move factor: 0.00')
            # distance is view vector based
            self.view_vector = Vector((0,0,-1))
            self.view_vector.rotate(self.cam.matrix_world)
        else:
            # object mode
            if self.object_Mode:
                # print(f"{self.objects[0].matrix_world.translation=}")
                self.init_vecs = [o.matrix_world.translation - self.cam_pos for o in self.objects]
                self.init_dists = [v.length for v in self.init_vecs]
            # edit mode
            else:

                self.init_wPos = []
                for v in self.objects:
                    # wPos
                    self.init_wPos.append( self.odata.verts[v].co )

                # self.init_vecs = []
                # for v in self.objects:
                #     co_final = self.objects_in_obj_mode.diff @ v.co
                #     print(f"{co_final=}")
                #     print(f"{co_final - self.cam_pos=}")
                #     self.init_vecs.append( co_final - self.cam_pos )
                pass

            
            context.area.header_text_set(f'Move factor: 0.00 | Mode: {self.mode} (M to switch) :) ')

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}

    def modal(self, context, event):

        # record again position in bmesh if edit
        if not self.object_Mode:
            bpy.ops.object.mode_set(mode='OBJECT')
            bpy.ops.object.mode_set(mode='EDIT')
            # -----------
            listver = []
            # bmesh object
            object = bpy.context.active_object
            self.object = object
            self.odata = bmesh.from_edit_mesh(self.object.data)
            self.odata.select_flush(False)
            self.odata.verts.ensure_lookup_table()

            # -----------------
            # if the vertex is selected add to the list
            for vertice in self.odata.verts[:]:
                if vertice.select:
                    listver.append(vertice.index)

            self.objects = listver

        if self.mode == 'distance':
            factor = 0.1
            if event.shift:
                factor = 0.01
        
        else:
            # Smaller factor for proportional dist
            factor = 0.01
            if event.shift:
                factor = 0.001

        if event.type in {'MOUSEMOVE'}:
            diff = (event.mouse_x - self.init_mouse_x) * factor
            if not self.object_Mode:
                self.init_mouse_x = event.mouse_x
                diff += 1

            # print("-***", diff)

            if self.cam.data.type == 'ORTHO':
                # just push in view vector direction
                context.area.header_text_set(f'Move factor: {diff:.2f}')
                for i, obj in enumerate(self.objects):
                    new_vec = self.init_mats[i].translation + (self.view_vector * diff)
                    obj.matrix_world.translation = new_vec
            else:
                # Push from camera point and scale accordingly
                context.area.header_text_set(f'Move factor: {diff:.2f} | Mode: {self.mode} (M to switch) -:)) ')

                for i, obj in enumerate(self.objects):
                    if self.mode == 'distance':
                        ## move with the same length for everyone 
                        if self.object_Mode: new_vec = self.init_vecs[i] + (self.init_vecs[i].normalized() * diff)
                    
                    else:
                        ## move with proportional factor from individual distance vector to camera
                        if self.object_Mode: new_vec = self.init_vecs[i] + (self.init_vecs[i] * diff)

                    # object mode
                    if self.object_Mode:
                        obj.matrix_world.translation = self.cam_pos + new_vec
                    # edit mode
                    else:
                        for v in self.objects:
                            # wPos
                            wPos =  self.object.matrix_world @ self.odata.verts[v].co 
                            # cam map
                            wPos = (wPos - self.cam_pos) * diff + self.cam_pos
                            # back to local                            
                            lPos = self.object.matrix_local.inverted() @ wPos
                            # go again
                            self.odata.verts[v].co = lPos

                    # scale
                    if self.object_Mode:
                        dist_percentage = new_vec.length / self.init_dists[i]
                        obj.scale = self.init_mats[i].to_scale() * dist_percentage

        if event.type in {'M'} and event.value == 'PRESS':
            # Switch mode
            self.mode = 'distance' if self.mode == 'proportional' else 'proportional'

        if event.type in {'LEFTMOUSE'} and event.value == 'PRESS':
            context.area.header_text_set(None)
            return {"FINISHED"}
        
        if event.type in {'RIGHTMOUSE', 'ESC'} and event.value == 'PRESS':
            for i, obj in enumerate(self.objects):
                if self.object_Mode:
                    obj.matrix_world = self.init_mats[i]
                else:
                    # it doesnt work
                    # for i, v in enumerate(self.objects):
                    #     # wPos
                    #     self.odata.verts[v].co = self.init_wPos[i]
                    pass
                   
            context.area.header_text_set(None)
            return {"CANCELLED"}

        return {"RUNNING_MODAL"}

""" # Own standalone panel
class ODM_PT_sudden_depth_panel(bpy.types.Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Gpencil"
    bl_label = "Depth move"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.operator('object.depth_proportional_move', text='Depth move', icon='TRANSFORM_ORIGINS')
"""


### --- REGISTER ---

classes=(
PUP_Gp_OP_depth_cam_move,
)

def menu_func(self, context):
    self.layout.operator(PUP_Gp_OP_depth_cam_move.bl_idname)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
        bpy.types.VIEW3D_MT_object.append(menu_func) 
        bpy.types.VIEW3D_MT_edit_mesh_vertices.append(menu_func) 

def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)