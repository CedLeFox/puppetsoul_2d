# bl_info = {
#     "name": "PUP Gp project stroke",
#     "blender": (3, 4, 0),
#     "version": (0, 1),
#     "author": "Cedric NICOLAS www.cedriclefox.com",
#     "category": "Object",
# }

"""
Project strokes to a surface, respecting the thickness compensation with the projection

todo
- numpy
- make all frame work

By Cedric3d
nicolasced(AT)gmail.com
www.puppetsoul.com
www.cedriclefox.com

"""

import bpy
from mathutils import Vector
import mathutils
import math
import numpy as npy


class PUP_Gp_OP_project_stroke(bpy.types.Operator):
    """Operator to reproject and keeping the same size"""
    bl_idname = "object.pup_gp_project_stroke"
    bl_label = "PUP Gp - Project Stroke (Camera Mapping)"
    bl_options = {'REGISTER', 'UNDO'}
    
    apply_to_selected: bpy.props.BoolProperty(
        name="Selected Obj Only",
        default=True,
        options={'SKIP_SAVE'}
    )
    
    apply_only_to_unlock_layers: bpy.props.BoolProperty(
        name="Unlock Layers Only",
        default=True,
        options={'SKIP_SAVE'}
    )
    
    apply_only_to_selected_layer: bpy.props.BoolProperty(
        name="Selected Layer Only",
        default=True,
        options={'SKIP_SAVE'}
    )
    

    current_frame_only: bpy.props.BoolProperty(
        name="Current Frame Only (vs Selected ones)",
        default=True,
        options={'SKIP_SAVE'}
    )

    offset: bpy.props.FloatProperty(
        name="projection offset",
        default=0.1,
        min=0.0,
        max=10.0,
        options={'SKIP_SAVE'}
    )
   
    factor_mult: bpy.props.FloatProperty(
        name="factor mult",
        default=1.0,
        min=0.0,
        max=10.0,
        options={'SKIP_SAVE'}
    )   

    general_mult: bpy.props.FloatProperty(
        name="global mult",
        default=1,
        min=0.0,
        max=10.0,
        options={'SKIP_SAVE'}
    )   


    near_mult: bpy.props.FloatProperty(
        name="near mult",
        default=1,
        min=0.0,
        max=10.0,
        options={'SKIP_SAVE'}
    )  

    mid_mult: bpy.props.FloatProperty(
        name="mid mult",
        default=1,
        min=0.0,
        max=10.0,
        options={'SKIP_SAVE'}
    )       

    far_mult: bpy.props.FloatProperty(
        name="far mult",
        default=1,
        min=0.0,
        max=10.0,
        options={'SKIP_SAVE'}
    )   


    off: bpy.props.BoolProperty(
        name="off",
        default=True,
        options={'SKIP_SAVE'}
    )  

    debug: bpy.props.BoolProperty(
        name="debug",
        default=False,
        options={'SKIP_SAVE'}
    )        
    @classmethod
    def poll(cls, context):
        return context.selected_objects is not None
    
    def draw(self, context):
        layout = self.layout
        col0 = layout.column()
        col0.label(text="Apply to:")
        col1 = layout.column()
        col1.prop(self, "apply_to_selected")
        col1.prop(self, "apply_only_to_unlock_layers")
        col2 = layout.column()
        col2.prop(self, "apply_only_to_selected_layer")
        col2.label(text=" ")
        col2.prop(self, "current_frame_only")
        col3 = layout.column()
        col3.prop(self, "offset")
        col3.label(text=" ")
        col3.prop(self, "factor_mult")
        col3.prop(self, "general_mult")
        col4 = layout.column()
        col4.label(text=" ")
        col4.prop(self, "near_mult")
        col4.prop(self, "mid_mult")
        col4.prop(self, "far_mult") 
        col4.label(text=" ")       
        col4.prop(self, "off")
        col4.prop(self, "debug")

    
        # If the apply_only_to_selected_layer property is True, gray out the other two properties
        if self.apply_only_to_selected_layer:
            col1.enabled = False
        else:
            col1.enabled = True

        # force mode
        if bpy.context.mode == "EDIT_GPENCIL":
            col1.enabled = False
            col2.enabled = False

            
    
    # ---------------------------------------------------
    def execute(self, context):
        # Define the proj stroke
        def proj_stroke(gpencil: bpy.types.GreasePencil, offset: float, factor_mult:float, general_mult:float, near_mult:float, mid_mult:float, far_mult:float, current_frame_only: bool, debug: bool):
            
            # get the current camera
            cam = bpy.context.scene.camera
            if not cam:
                self.report({'ERROR'}, 'No active camera')
                return {"CANCELLED"}

            cam_pos = cam.matrix_world.translation

            # get frame            
            scene = bpy.context.scene
            current_frame = scene.frame_current
            current_frame = int(current_frame)
            #print("FRANE", current_frame)

            # if near mult 
            nearFar_mult = False
            if near_mult != 1.0 or far_mult != 1.0 or mid_mult != 1.0:
                nearFar_mult = True
            # -----------------------------------------------------
            # first record all strokes that need to be done
            selected_strokes = []

            for layer in gpencil.layers:

                # because layer.select doesnt work
                current_layer = bpy.context.active_object.data.layers.active.info
                is_layer_selected = False
                if current_layer == layer.info:
                    is_layer_selected = True

                if debug: print(" - Check Layer : ", gpencil.name, " > ", layer, "| sel/lock : ",  is_layer_selected, layer.lock)

                # -------------------------------
                # multi conditional process
                doIt = False
                if (self.apply_only_to_unlock_layers and not self.apply_only_to_selected_layer and not layer.lock) or (self.apply_only_to_selected_layer and is_layer_selected) or (not self.apply_only_to_selected_layer and (  (self.apply_only_to_unlock_layers and not layer.lock) or not self.apply_only_to_unlock_layers) ) :
                    doIt = True

                # if we are in edit mode, then process and check later if we have to
                if bpy.context.mode == "EDIT_GPENCIL":
                    doIt = True                    
                
                # process for now
                if doIt:
                    if debug: print(" ---> Process! ")

                    for it, frame in enumerate(layer.frames):

                        frame_do = False
                        if current_frame_only:
                            if frame.frame_number == current_frame or (it == len(layer.frames)-1 and current_frame > layer.frames[-1].frame_number):
                                frame_do = True
                            elif frame.frame_number < current_frame and current_frame < layer.frames[it+1].frame_number:
                                frame_do = True
                            else:
                                frame_do = False
                        else:
                            frame_do = True

                        # if proceed to frame
                        if frame_do:
                            if not current_frame_only:
                                print("camera mapping (prep) frame : ", frame.frame_number," | On layer : ", layer.info)
                            for stroke in frame.strokes:

                                # if we r here, we check if we r in edit gp AND unselected. Then we dont process it.
                                #   if we r heree, do it is true by fefault
                                doIt = True
                                if stroke.select == False and bpy.context.mode == "EDIT_GPENCIL":
                                    doIt = False
                                # as the project op is sensitive to selection, manually select stroke founded here to be able to be projected
                                if bpy.context.mode != "EDIT_GPENCIL":
                                    stroke.select = True

                                # finally do it
                                if doIt:
                                    # print(stroke)
                                    selected_strokes.append(stroke)
                                    stroke.select = False

            # print("selected stroke", selected_strokes)

            for layer in gpencil.layers:

                # because layer.select doesnt work
                current_layer = bpy.context.active_object.data.layers.active.info
                is_layer_selected = False
                if current_layer == layer.info:
                    is_layer_selected = True

                if debug: print(" - Check Layer : ", gpencil.name, " > ", layer, "| sel/lock : ",  is_layer_selected, layer.lock)

                # -------------------------------
                # multi conditional process
                doIt = False
                if (self.apply_only_to_unlock_layers and not self.apply_only_to_selected_layer and not layer.lock) or (self.apply_only_to_selected_layer and is_layer_selected) or (not self.apply_only_to_selected_layer and (  (self.apply_only_to_unlock_layers and not layer.lock) or not self.apply_only_to_unlock_layers) ) :
                    doIt = True

                # if we are in edit mode, then process and check later if we have to
                if bpy.context.mode == "EDIT_GPENCIL":
                    doIt = True                    
                
                # process for now
                if doIt:
                    if debug: print(" ---> Process! ")

                    for it, frame in enumerate(layer.frames):

                        frame_do = False
                        if current_frame_only:
                            if frame.frame_number == current_frame or (it == len(layer.frames)-1 and current_frame > layer.frames[-1].frame_number):
                                frame_do = True
                            elif frame.frame_number < current_frame and current_frame < layer.frames[it+1].frame_number:
                                frame_do = True
                            else:
                                frame_do = False
                        else:
                            if frame.select:
                                frame_do = True
                            # print(frame.select)

                        # if proceed to frame
                        if not current_frame_only:
                            # print("camera mapping frame : ", frame.frame_number)
                            print("camera mapping (exec) frame : ", frame.frame_number," | On layer : ", layer.info)

                        # for frame in layer.frames:
                        if frame_do:
                            if not current_frame_only:
                                scene.frame_current = frame.frame_number
                            for stroke in frame.strokes:

                                # if we r here, we check if we r in edit gp AND unselected. Then we dont process it.
                                #   if we r heree, do it is true by fefault
                                # doIt = True
                                # if stroke.select == False and bpy.context.mode == "EDIT_GPENCIL":
                                #     doIt = False
                                # # as the project op is sensitive to selection, manually select stroke founded here to be able to be projected
                                # if bpy.context.mode != "EDIT_GPENCIL":
                                #     stroke.select = True

                                doIt = False
                                if stroke in selected_strokes:
                                    doIt = True
                                    # print("STROKE FOUNDED")


                                # finally do it
                                if doIt:
                                    # select the stroke because we will project ir
                                    stroke.select = True
                                    # print("DO IT", stroke)
                                    # init all
                                    pt_pos_list = []
                                    pt_len_list = []
                                    init_vect = []
                                    init_mat = []

                                    # get 1st list of points before get move
                                    for point in stroke.points:
                                        # print(" scdale or whaever >", point.)
                                        # list of all pos
                                        pt_pos_list.append(Vector(point.co))
                                        # init vector from the camera and point
                                        init_vect.append( Vector(point.co - cam_pos ))
                                        # init matrix from the point itself
                                        init_mat.append( mathutils.Matrix.Translation(Vector(point.co)))


                                    # project on surface
                                    bpy.ops.gpencil.reproject(type='SURFACE',  offset=offset)
                                    stroke.select = False
                                    # recalculate the length for all points
                                    for i, point in enumerate(stroke.points):
                                        # get length btw orig and moved point
                                        length = (point.co - pt_pos_list[i]).length
                                        if nearFar_mult: pt_len_list.append(float(length))

                                        # get dist with cam
                                        new_vect = Vector(point.co - cam_pos )
                                        # factor with old verctor
                                        dist_percentage = new_vect.length / init_vect[i].length
                                        # deduct scale
                                        scale = init_mat[i].to_scale() * dist_percentage
                                        # get only one el 
                                        factor = float(scale.x)
                                        factor = factor * factor_mult

                                        # modify the all scale with the general mult and the factor_mult
                                        pf = point.pressure * general_mult
                                        point.pressure = pf * factor

                                    # if near mult 
                                    if nearFar_mult:

                                        # go ahead
                                        # get the max dist
                                        # print("L")
                                        # print(pt_len_list)
                                        # print(pt_len_list[0])
                                        length_max = max(pt_len_list)
                                        length_min = min(pt_len_list)

                                        # print("minmax : ", length_min, " / ", length_max)

                                        for i, point in enumerate(stroke.points):
                                            pf = float(point.pressure)
                                            pos_in_range = npy.interp(pt_len_list[i],[length_min,length_max],[0,1])


                                            if pos_in_range<0.5:
                                                final_pos_in_range = pos_in_range * 2
                                                final_near_mult = near_mult
                                                final_far_mult = mid_mult
                                            else:
                                                final_pos_in_range = (pos_in_range - 0.5) * 2
                                                final_near_mult = mid_mult
                                                final_far_mult = far_mult                                         


                                            far_mult_factor = (final_pos_in_range * final_far_mult) + ((1-final_pos_in_range) * 1)
                                            near_mult_factor = ((1-final_pos_in_range) * final_near_mult) + (final_pos_in_range * 1)


                                            pf = pf * far_mult_factor * near_mult_factor

                                            point.pressure = pf


                                    pt_pos_list = []
                                    pt_len_list = []


            # finally select back the strokes
            for each in selected_strokes:
                each.select = True
            
            if not current_frame_only:
                scene.frame_current = current_frame

        if self.off:
            self.report({'WARNING'}, 'Off!')
            return {'FINISHED'}

        # If the apply_to_selected property is True, remove the vertex colors from the selected objects
        count_process = 0

        # for now!
        # self.all_frames = True

        if self.debug : 
            print("---START PROJECTION---")
            print("only to sel : ", self.apply_to_selected)

        if bpy.context.mode == "EDIT_GPENCIL":
            print("Edit gp mode detected. Force solo selection")
            self.apply_to_selected = True

        if self.apply_to_selected:
            for obj in context.selected_objects:
                if obj.type == 'GPENCIL':
                    # print("sel ", obj.data)
                    proj_stroke(obj.data, self.offset, self.factor_mult, self.general_mult, self.near_mult, self.mid_mult, self.far_mult, self.current_frame_only, self.debug)
                    count_process += 1
        # Otherwise, remove the vertex colors from all objects
        else:
            for gp in bpy.data.grease_pencils:
                # print("all", gp)
                proj_stroke(gp, self.offset, self.factor_mult, self.general_mult, self.near_mult, self.mid_mult, self.far_mult, self.current_frame_only, self.debug)
                count_process += 1
                
        if count_process == 0:
            self.report({'WARNING'}, 'No stroke to process')
        else:
            self.report({'INFO'}, ('Process '+ str(count_process)+ " strokes"))
        
        return {'FINISHED'}

# Register the operator
# bpy.utils.register_class(PUP_Gp_OP_project_stroke)

def menu_func(self, context):
    self.layout.operator(PUP_Gp_OP_project_stroke.bl_idname)

def register():
    bpy.utils.register_class(PUP_Gp_OP_project_stroke)
    bpy.types.VIEW3D_MT_object.append(menu_func)  # Adds the new operator to an existing menu.
    bpy.types.VIEW3D_MT_draw_gpencil.append(menu_func) 
    bpy.types.VIEW3D_MT_edit_gpencil_stroke.append(menu_func) 

def unregister():
    bpy.utils.unregister_class(PUP_Gp_OP_project_stroke)