# bl_info = {
#     "name": "PUP Gp select by color",
#     "blender": (3, 4, 0),
#     "version": (0, 1),
#     "author": "Cedric NICOLAS www.cedriclefox.com",
#     "category": "Object",
# }


"""
Select stroke by color


By Cedric3d
nicolasced(AT)gmail.com
www.puppetsoul.com
www.cedriclefox.com

"""

import bpy

class PUP_Gp_OP_select_by_color(bpy.types.Operator):
    """Operator to change the vertex colors from a Grease Pencil object"""
    bl_idname = "object.pup_gp_select_by_color"
    bl_label = "PUP Gp - Select by color"
    bl_options = {'REGISTER', 'UNDO'}
    
    apply_only_to_unlock_layers: bpy.props.BoolProperty(
        name="Unlock Layers Only",
        default=True,
        options={'SKIP_SAVE'}
    )
    
    apply_only_to_selected_layer: bpy.props.BoolProperty(
        name="Selected Layer Only",
        default=True,
        options={'SKIP_SAVE'}
    )
    
    thr_R: bpy.props.FloatProperty(
        name="Thresold R",
        default=0.01,
        min=0.0,
        max=1,
        options={'SKIP_SAVE'}
    )
    thr_G: bpy.props.FloatProperty(
        name="Thresold G",
        default=0.01,
        min=0.0,
        max=1.0,
        options={'SKIP_SAVE'}
    )
    thr_B: bpy.props.FloatProperty(
        name="Thresold B",
        default=0.01,
        min=0.0,
        max=1.0,
        options={'SKIP_SAVE'}
    )    

    select_stroke: bpy.props.BoolProperty(
        name="Select Strokes",
        default=True,
        options={'SKIP_SAVE'}
    )

    all_frames: bpy.props.BoolProperty(
        name="all frames",
        default=False,
        options={'SKIP_SAVE'}
    )

    debug: bpy.props.BoolProperty(
        name="debug",
        default=False,
        options={'SKIP_SAVE'}
    )        
    @classmethod
    def poll(cls, context):
        return context.selected_objects is not None
    
    def draw(self, context):
        layout = self.layout
        col0 = layout.column()
        col0.label(text="Apply to:")
        col0.prop(self, "apply_only_to_unlock_layers")
        col0.prop(self, "apply_only_to_selected_layer")
        col0.prop(self, "thr_R")
        col0.prop(self, "thr_G")
        col0.prop(self, "thr_B")
        col0.prop(self, "select_stroke")
        col0.prop(self, "all_frames")

        
        col0.prop(self, "debug")

    # main
    def execute(self, context):
        # Define the remove_vertex_paint function
        def select_by_color(gpencil: bpy.types.GreasePencil, target_color:list, to_stroke: bool, all_frames: bool, thresold_r:float, thresold_g:float, thresold_b:float, debug: bool):
            
            # init
            select_this_stroke=False

            # process
            for layer in gpencil.layers:
                if debug: print(" - Check Layer : ", gpencil.name, " > ", layer, "| sel/lock : ",  layer.select, layer.lock)

                # because layer.select doesnt work
                current_layer = bpy.context.active_object.data.layers.active.info
                is_layer_selected = False
                if current_layer == layer.info:
                    is_layer_selected = True

                if debug: print(" - Check Layer : ", gpencil.name, " > ", layer, "| sel/lock : ",  is_layer_selected, layer.lock)


                if (self.apply_only_to_unlock_layers and not self.apply_only_to_selected_layer and not layer.lock) or (self.apply_only_to_selected_layer and is_layer_selected) or (not self.apply_only_to_selected_layer and (  (self.apply_only_to_unlock_layers and not layer.lock) or not self.apply_only_to_unlock_layers) ) :
                    if debug: print(" ---> Process! ")

                    # define frames
                    frames = []
                    if all_frames:
                        frames = [gp_layer.active_frame]
                    else:
                        frames = layer.frames

                    for frame in frames:
                        for stroke in frame.strokes:
                            #stroke.vertex_color_fill = [0.0, 0.0, 0.0, 0.0]
                            i = 0
                            for point in stroke.points:

                                # actual color
                                cl = point.vertex_color
                                cl=list(cl)

                                # get point in colors thresold                           
                                if ( abs(target_color[0] - cl[0]) < thresold_r ) and ( abs(target_color[1] - cl[1]) < thresold_g ) and ( abs(target_color[2] - cl[2]) < thresold_b ):
                                    
                                    # select stroke or points
                                    if to_stroke:
                                        select_this_stroke = True
                                        break
                                    else:                                    
                                        point.select = True

                            # if select strokes
                            if select_this_stroke:
                                stroke.select = True
                                select_this_stroke = False
                                bpy.context.scene.tool_settings.gpencil_selectmode_edit = 'STROKE'

                                #print(stroke.layer)
 
                                # 
                                #if move_to_layer:
                                #    bpy.context.scene.tool_settings.gpencil_selectmode_edit = 'STROKE'

                                #    bpy.ops.gpencil.move_to_layer[paste_layer_name]    
                                #    bpy.context.scene.tool_settings.gpencil_selectmode_edit = 'POINT'

                                    #self.move_stroke(gpencil, layer, paste_layer_name, stroke)                     




        # Get the active grease pencil layer
        gp_layer = bpy.context.active_gpencil_layer
        # Get the active frame for the layer
        frame = gp_layer.active_frame
        # Create an empty list to store the selected points
        selected_points = []

        # Iterate over all strokes in the frame
        for stroke in frame.strokes:
            # Iterate over all points in the stroke
            for point in stroke.points:
                # Check if the point is selected
                if point.select:
                    # Add the point to the list of selected points
                    selected_points.append(point)

        if self.debug:
            print("selected points : ", selected_points)

        print(len(selected_points))

        if len(selected_points) == 0:
            self.report({'ERROR'}, 'No founded point. Selected point should be in the selected layer')
            return {'FINISHED'}

        # Print the list of selected points
        target_color = list(selected_points[0].vertex_color)

        if self.debug: print("Target Color to match : ", list(target_color))

        # --
        newColors = []
        for i in range(len(list(target_color))):
            cl = list(target_color)[i] ** 1/1.2
            newColors.append( cl )

        if self.debug: print("Target Color to match gammaed: ", list(newColors))

        active_object = bpy.context.active_object
        if active_object.type == 'GPENCIL':
            select_by_color(active_object.data, target_color, self.select_stroke, self.all_frames, self.thr_R, self.thr_G, self.thr_B, self.debug)

        return {'FINISHED'}

# Register the operator
# bpy.utils.register_class(ChangeVertexColorsOperator)

def menu_func(self, context):
    self.layout.operator(PUP_Gp_OP_select_by_color.bl_idname)

def register():
    bpy.utils.register_class(PUP_Gp_OP_select_by_color)
    #bpy.types.VIEW3D_MT_object.append(menu_func)  # Adds the new operator to an existing menu.
    bpy.types.VIEW3D_MT_select_gpencil.append(menu_func) 

def unregister():
    bpy.utils.unregister_class(PUP_Gp_OP_select_by_color)